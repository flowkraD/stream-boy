from selenium import webdriver
import os
import time

#Links we will swap between to avoid YT algorithm snooping in on continuous stream
link1 = "https://www.youtube.com/watch?v=Amq-qlqbjYA" #BLACKPINK - '마지막처럼 (AS IF IT'S YOUR LAST)' M/V
link2 = "https://www.youtube.com/watch?v=ybGOT4d2Hs8" #NAPALM DEATH - You Suffer

#Selenium to open browser and automate the play
driver = webdriver.Chrome()
#In case yo urun into driver issue download drivers from https://chromedriver.storage.googleapis.com/index.html?path=2.41/
#Unzip and link the path to the file as shown below and you are good to go
#driver = webdriver.Chrome(executable_path="/your/driver/path/here")

t1 = 216    #Duration of video 1
t2 = 3      #Duration of video 2

for i in range(1,254):
    #Changing IP every 299 views to put off YT algorithm
    ip = "192.168.15."+str(i)
    os.system("networksetup -setmanualwithdhcprouter Wi-Fi "+ip)    #Works only on OSX
    #opening links and waiting until the video is finished
    for j in range(1,300):
        driver.get(link1)
        time.sleep(t1)
        driver.get(link2)
        time.sleep(t2)
    
        
