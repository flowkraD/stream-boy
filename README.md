# Stream Boy
<br />
Note: Currently works only on OsX. More support is on it's way
<br />
<br />
Are you a soldier of KPOP and are tired of manually switching between videos only to make sure each and every one of your view counts in the battle? Fear not BlinkBot is here to your rescue. All you need is Python 3.x and selenium and you are good to go. Fighting war one loop at a time.

### How to run this amazing script?
Just go to your terminal and type:
```
pip3 install selenium
```
Boom! Everything this little guy needs is installed.
now all you have to do is:
```
python3 blink.py
```
And the script takes care of everything else.

### Want to change songs?
Just change the URL in blink.py and you are all set.

<br />
In times like this, small efforts make big difference. We are here to make the war less clicky!

